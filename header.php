<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>PHP</title>
        <link rel = "stylesheet" href = "css/bootstrap.min.css">
        <link rel = "stylesheet" href = "css/style.css">
        <script src = "jquery-3.2.1.min.js"></script> 
        <script src = "Chart.bundle.min.js"></script>  
        <script src = "moment.js"></script> 
        <script src = "script.js"></script>

        <nav class = "navbar navbar-inverse">
            <div class = "container-fluid">
                <div class = "navbar-header">
                    <a class = "navbar-brand" href = "index.php">Home</a><br>
                </div>
                <ul class = "nav navbar-nav">
                <li><a href = "jobposts.php">Jobs</a></li>
                <?php
                    if(isset($_SESSION['login_user']) && $_SESSION['login_user'] != "admin"){
                    echo '<li><a href = "makepost.php">Make a post</a></li>';
                    }
                ?>
                    </ul>
                <ul class = "nav navbar-nav navbar-right">
                <li><a href = "search.php">Search</a></li>';
                <?php
                if(isset($_SESSION['login_user']) && $_SESSION['login_user']){
                    if($_SESSION['login_user'] != "admin"){
                        echo '<li><a href = "profile.php?id='.$_SESSION['id'].'">'.$_SESSION['login_user'].'</a></li>';
                    }
                    
                    echo '<li><a href = "logout.php">Logout</a></li>';
                }        
                else
                {
                    echo '<li><a href = "login.php">Login</a></li>';
                    echo '<li><a href = "register.php">Register</a></li>';
                }
            ?>
                </ul>
            </div>
        </nav>
    </head>
<body>