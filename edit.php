<?php
    session_start();
    include('header.php');
    require('db.php');
    
    if(!empty($_POST['editDescriptionButton'])){
        foreach($_POST as $key=>$value) {
            if(empty($_POST[$key])) {
            $error_message = "All Fields are required";
            break;
            }

            if(!isset($error_message)){
                $insertDescription = $_POST['inputDescription']; 
                $userid = $_SESSION['id'];
                $insertQuery = "insert into description(description, account_id) values('$insertDescription', $userid) on duplicate key update description = '$insertDescription'";
                $insertIntoDescription = $conn->query($insertQuery);
                if($insertDescription){
                    $success_message = "Added description";
                }else{
                    $error_message = "Could not add description";
                }
            }
        }
    }
    
    $descriptionQuery = "select * from description where account_id =".$_SESSION['id'];
    $descriptionResult = $conn->query($descriptionQuery);
    if($descriptionResult != False){
    $description = mysqli_fetch_assoc($descriptionResult);
    }else{
        $description['description'] = "";
    }

?>

<form name = "editDescriptionForm" class = "form-signin" action = "" method = "POST">       
    <?php if(!empty($error_message)) { ?>	
		<div class="error-message"><?php if(isset($error_message)) echo $error_message; ?></div>
	<?php } ?>
    <?php if(!empty($success_message)) { ?>	
		<div class="error-message"><?php if(isset($success_message)) echo $success_message; ?></div>
	<?php } ?>
    <textarea class = "form-control" rows = "10" placeholder = "Add description here" name = "inputDescription"><?php echo $description['description'] ?></textarea><br>
    <input type = "submit" class = "btn btn-primary" name = "editDescriptionButton">
</form>

<?php
    include('footer.php');
?>