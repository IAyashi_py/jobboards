<?php
session_start();
require('db.php');
include('header.php');

if($_SESSION['login_user'] != "admin"){
    header("location: index.php");
}

if(!empty($_POST['visibility'])){
    $visibility = $_POST['visibility'];
    $postid = $_POST['postid'];
    if($visibility == 'Hidden'){
        $conn->query("update posts set visible = 1 where id = '$postid'");
        
    }else{
        $conn->query("update posts set visible = 0 where id = '$postid'");
    }
}

$query = $conn->query("select * from posts");

    if($query->num_rows > 0){
        ?>
        <div class = "container">
        <table class = "table" border = 1>
        <tr>
        <th> Job Title</th>
        <th> Deadline </th>
        <th> Visit Count </th>
        <th> Visibility </th>
        <th> Delete </th>
        </tr>
        <?php
        while($row = $query->fetch_assoc()){
            $datetime1 = date_create($row['deadline']);
            $userid = $row['id'];
            $usertitle = $row['title'];
            $datetime2 = date_create(date('Y-m-d'));
            $interval = date_diff($datetime2, $datetime1);
            echo "<tr>";
            echo "<td><a href = 'postdetail.php?id=".$userid."'>".$usertitle."</a></td>";
            if($interval->format('%R') == '+'){
                echo "<td>".$interval->format('%a days')."</td>";
            }else{
                echo "<td>Deadline Passed</td>";
            }
            echo "<td>".$row['visitcount'];
            if($row['visible'] == 1){
                echo "<td>";
                ?>
            <form name = "f" action = "" method = "post">
                <input type = "hidden" name = "postid" value = "<?php echo $row['id'] ?>">
                <input type = "submit" value = "Visible" name = "visibility">
            </form>
            <?php
            }else{
                echo "<td>";
                ?>
                <form name = "f" action = "" method = "post">
                    <input type = "hidden" name = "postid" value = "<?php echo $row['id'] ?>">
                    <input type = "submit" value = "Hidden" name = "visibility">
                </form>
                <?php
            }
            echo "</td>";
            echo '<td><a href = delete.php?id='.$row['id'].'> Delete </a></td>';      
            echo "</tr>";

        }
        echo "</table>";
    }

    echo '<a href = "chart.php">View Chart</a><br>';
    echo '<a href = "users.php">View users</a>';
?>

