<?php
    session_start();
    include('header.php');
    require('db.php');
    if(!empty($_POST['change-password'])){
        $currentPassword = $_POST['password'];
        $new_password = $_POST['new_password'];
        $confirm_new_password = $_POST['confirm_new_password'];
        $username = $_POST['username'];

        foreach($_POST as $key=>$value) {
            if(empty($_POST[$key])) {
            $error_message = "All Fields are required";
            break;
            }
        }

        if(!isset($error_message)){
            if($username != $_SESSION['login_user']){
                $error_message = "Please enter your actual username";
            }
        }

        if(!isset($error_message)){
            $query = mysqli_query($conn, "select * from account where username = '$username'");
            $userResult = mysqli_fetch_assoc($query);
            $rows = mysqli_num_rows($query);
            if ($rows != 1) {
                if(password_verify($password, $userResult['password'])){
                    $error_message = "Password is invalid";
                }
            }
        }

        if(!isset($error_message)){
            if($new_password != $confirm_new_password){ 
                $error_message = 'Passwords should be same'; 
            }
        }   

        if(!isset($error_message)){
            $options = ['cost' => 12];
            $hashedpassword = password_hash($new_password, PASSWORD_DEFAULT, $options);
            $query = "UPDATE account SET password = '$hashedpassword' WHERE username = '$username'";
            $conn->query($query);
            $success_message = "Password changed successfully";
        }
    }
    
?>


<form name = "f" class = "form-signin   " action = "" method = "post">
    <?php if(!empty($error_message)) { ?>	
        <div class="error-message"><?php if(isset($error_message)) echo $error_message; ?></div>
    <?php } ?>

    <?php if(!empty($success_message)) { ?>	
        <div class="success-message"><?php if(isset($success_message)) echo $success_message; ?></div>
    <?php } ?>

    Username: <input name = "username" class = "form-control" type = "text"><br>
    Current Password: <input name = "password" class = "form-control" type = "password"><br>
    New Password:<input name = "new_password" class = "form-control" type = "password"><br>
    Confirm New Password:<input name = "confirm_new_password" class = "form-control" type = "password"><br>
    <input type = "submit" class = "btn btn-primary" name = "change-password">
</form>

