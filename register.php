<?php
include('header.php');
/* Form Required Field Validation */
if(!empty($_POST["register-user"]))
    {
    require('db.php');
    foreach($_POST as $key=>$value) {
        if(empty($_POST[$key])) {
        $error_message = "All Fields are required";
        break;
        }
    }
    /* Password Matching Validation */
    if($_POST['password'] != $_POST['confirm_password']){ 
    $error_message = 'Passwords should be same<br>'; 
    }

    if(!isset($error_message)) {
        $userName = $_POST["userName"];
        $query = "SELECT username FROM account WHERE username = '$userName'";
        $result = mysqli_query($conn, $query);
        $count = mysqli_num_rows($result);
        if($count!=0){
            $error_message = "Provided username is already in use.";
        }
    }


    /* Email Validation */
    if(!isset($error_message)) {
        if (!filter_var($_POST["userEmail"], FILTER_VALIDATE_EMAIL)) {
        $error_message = "Invalid Email Address";
        }else{
            $userEmail = $_POST["userEmail"];
            $query = "SELECT email FROM account WHERE email = '$userEmail'";
            $result = mysqli_query($conn, $query);
            $count = mysqli_num_rows($result);
            if($count!=0){
                $error_message = "Provided Email is already in use.";
            }
        }   
    }


    /* Validation to check if gender is selected */
    if(!isset($error_message)) {
    if(!isset($_POST["gender"])) {
    $error_message = " All Fields are required";
    }
    }

    /* Validation to check if Terms and Conditions are accepted */

    if(!isset($error_message)){

        $options = ['cost' => 12];
        $userName = $_POST["userName"];
        $firstName = $_POST["firstName"];
        $lastName = $_POST["lastName"];
        $password = password_hash($_POST["password"], PASSWORD_DEFAULT, $options);
        $userEmail = $_POST["userEmail"];
        $gender = $_POST["gender"];
        $query = "INSERT INTO `account` (username, firstname, lastname, email, password, gender, role) values('$userName', '$firstName', '$lastName', '$userEmail', '$password' ,'$gender', 1)";
        $result = $conn->query($query);

        if($result){
            header("Location: login.php");
        }
        $conn->close();
    }
}
?>

<script type="text/javascript">

function validateGender(radio){
    for(var i = 0; i < radio.length; i++){
            if(radio[i].checked) return true;
        }
    return false;
}


function validate()
    {   
        var error="";
        var name = document.getElementById("name_of_user");
        valid = true;

        document.getElementById( "usernameError" ).innerHTML =
        document.getElementById( "firstnameError" ).innerHTML =
        document.getElementById( "lastnameError" ).innerHTML =
        document.getElementById( "emailError" ).innerHTML =
        document.getElementById( "passwordError" ).innerHTML =
        document.getElementById( "confirmError" ).innerHTML =
        document.getElementById( "genderError" ).innerHTML = "";
        
        if( name.value == "" ){
                error = " You Have To Enter a valid username. ";
                document.getElementById( "usernameError" ).innerHTML = error;
                valid = false;
                
            }

        var name = document.getElementById("first_name");
                error = " You Have To Write Your First Name. ";
        if( name.value == "" ){
                document.getElementById( "firstnameError" ).innerHTML = error;
                valid = false;
                
        }

        var name = document.getElementById("last_name");
        if( name.value == "" ){
                error = " You Have To Write Your Last Name. ";
                document.getElementById( "lastnameError" ).innerHTML = error;
                valid = false;
                
        }

        var email = document.getElementById( "email_of_user" );
        if( email.value == ""){
            error = "Please Enter your Email Address. ";
            document.getElementById( "emailError" ).innerHTML = error;
            valid = false;
        }else{
            if ((/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value)) != true){
                error = "It is not a valid email";
                document.getElementById( "emailError" ).innerHTML = error;
                valid = false;
            }
        }

        var password = document.getElementById( "password_of_user" );
        if( password.value == ""){
            error = "Enter password";
            document.getElementById( "passwordError" ).innerHTML = error;
            valid = false;
        }

        var confirmpassword = document.getElementById("confirm_password");
        if(confirmpassword.value != password.value){
            error = "Passworld doesnt match";
            document.getElementById("confirmError").innerHTML = error;
            valid = false;
        }

        if(validateGender(document.forms["frmRegistration"]["gender"]) == false){
            error = "Enter a gender";
            document.getElementById("genderError").innerHTML = error;
            valid = false;
        }

        return valid;
}

</script>   
<div id = "container">

<form id = "formRegistration" class = "form-signin" name="frmRegistration" method="post" action="" onsubmit="return validate(this);">
<?php if(!empty($success_message)) { ?>	
            <br>
            <div class = "panel panel-success">
                <div class = "panel-heading">Success</div>
		        <div class="success-message panel-body"><?php if(isset($success_message)) echo $success_message; ?></div>
                <?php ?>
            </div>
		    <?php } ?>
        <div id = "error_para">

        </div>

			<div class = "form-group">
                <label for = "username">User Name</label>
			    <input type="text" id = "name_of_user" class = "form-control" name="userName" value="<?php if(isset($_POST['userName'])) echo $_POST['userName']; ?>">
                <div id = "usernameError" style = "color:red"></div>
            </div>
            
            <div class = "form-group">
                <label>First Name</label>
                <input type="text" id = "first_name" class = "form-control" name="firstName" value="<?php if(isset($_POST['firstName'])) echo $_POST['firstName']; ?>"><br>
                <div id = "firstnameError" style = "color:red"></div>
            </div>
            
            <div class = "form-group">
                <label>Last Name</label>
			    <input type="text" id = "last_name" class = "form-control" name="lastName" value="<?php if(isset($_POST['lastName'])) echo $_POST['lastName']; ?>"><br>
                <div id = "lastnameError" style = "color:red"></div>
            </div>

            <div class = "form-group">
			    <label>Password</label>
			    <input type="password" id = "password_of_user" class = "form-control" name="password" value=""><br>
                <div id = "passwordError" style = "color:red"></div>
            </div>
            
            <div class = "form-group">
                <label>Confirm Password</label>
			    <input type="password" id = "confirm_password" class = "form-control" name="confirm_password" value=""><br>
                <div id = "confirmError" style = "color:red"></div>
            </div> 
            
            <div class = "form-group">
                <label>Email</label>
			    <input type="text" id = "email_of_user" class = "form-control" name="userEmail" value="<?php if(isset($_POST['userEmail'])) echo $_POST['userEmail']; ?>"><br>
                <div id = "emailError" style = "color:red"></div>
            </div>
            
            <div class = "form-group">
                <label>Gender
			    <input type="radio" id = "gender" name="gender" value="Male" <?php if(isset($_POST['gender']) && $_POST['gender']=="Male") { ?><?php  } ?>> Male
			    <input type="radio" id = "gender" name="gender" value="Female" <?php if(isset($_POST['gender']) && $_POST['gender']=="Female") { ?><?php  } ?>> Female<br>
                <div id = "genderError" style = "color:red"></div>
            </div>
			<input type="submit" class = "btn btn-primary" name="register-user" value="Register">
            <?php if(!empty($error_message)) { ?>	
            <br>
            <div class = "panel panel-danger">
                <div class = "panel-heading">Error</div>
		        <div class="error-message panel-body"><?php if(isset($error_message)) echo $error_message; ?></div>
                <?php $error_message = "" ?>
            </div>
		    <?php } ?>
</form>
</div>

<?php
include('footer.php');
?>