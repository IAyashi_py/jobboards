<?php
    require('db.php');
    $value = $_POST["send_tag"];
    $query = $conn->query("select * from posts where deadline > CURDATE() and visible = 1 and (title like '%$value%' or post like '%$value%')");
        if($query->num_rows > 0){
            ?>
            <table class = "table table-striped" border = 1>
            <tr>
            <th> Job Title</th>
            <th> Deadline </th>
            </tr>
            <?php
            while($row = $query->fetch_assoc()){
                $datetime1 = date_create($row['deadline']);
                $datetime2 = date_create(date('Y-m-d'));
                $id = $row['id'];
                $title = $row['title'];
                $interval = date_diff($datetime2, $datetime1);
                if($interval->format('%R') == '+' && $row['visible'] == 1){
                    echo "<tr>";
                    echo "<td><a href = 'postdetail.php?id=".$id."'>".$title."</a></td>";
                    echo "<td>".$interval->format('%a days')."</td>";
                    echo "</tr>";
                }
            }
            ?>
        </table>
        <?php
        }else{
            echo "Result not found";
        }
    ?>